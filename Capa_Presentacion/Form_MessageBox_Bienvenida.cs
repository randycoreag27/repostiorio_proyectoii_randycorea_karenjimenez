﻿using System;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_MessageBox_Bienvenida : Form
    {
        public Form_MessageBox_Bienvenida()
        {
            InitializeComponent();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form_Reportes_Administrador frm = new Form_Reportes_Administrador();
            frm.Show();
        }
    }
}
