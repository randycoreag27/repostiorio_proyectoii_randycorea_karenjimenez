﻿
namespace Capa_Presentacion
{
    partial class Form_Reportes_Administrador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Reportes_Administrador));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_salir = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_regresar = new System.Windows.Forms.PictureBox();
            this.Reporte1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.grafrepo3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label5 = new System.Windows.Forms.Label();
            this.grafico_reporte_cuatro = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label6 = new System.Windows.Forms.Label();
            this.cbx_provincias = new System.Windows.Forms.ComboBox();
            this.rb_masculino = new System.Windows.Forms.RadioButton();
            this.rb_femenino = new System.Windows.Forms.RadioButton();
            this.btn_generar_reporte_4 = new System.Windows.Forms.Button();
            this.grafico_reporte_dos = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.grafico_reporte_cinco = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_generar_reporte_cinco = new System.Windows.Forms.Button();
            this.cbx_provincia = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnFiltrar = new System.Windows.Forms.Button();
            this.dateTimeHasta = new System.Windows.Forms.DateTimePicker();
            this.dateTimeDesde = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.DataGridSextorepo = new System.Windows.Forms.DataGridView();
            this.Cedula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Genero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha_Nacimiento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipo_Vacuna = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad_Dosis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha_Segunda_Dosis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label12 = new System.Windows.Forms.Label();
            this.cbx_tipo_vacuna = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.btn_salir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_regresar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Reporte1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grafrepo3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grafico_reporte_cuatro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grafico_reporte_dos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grafico_reporte_cinco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSextorepo)).BeginInit();
            this.SuspendLayout();
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label11.Location = new System.Drawing.Point(15, 109);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 18);
            this.label11.TabIndex = 23;
            this.label11.Text = "Administrador";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label1.Location = new System.Drawing.Point(325, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(432, 80);
            this.label1.TabIndex = 24;
            this.label1.Text = "Reportes\r\n Registro de Vacunación";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_salir
            // 
            this.btn_salir.Image = ((System.Drawing.Image)(resources.GetObject("btn_salir.Image")));
            this.btn_salir.Location = new System.Drawing.Point(1010, 12);
            this.btn_salir.Name = "btn_salir";
            this.btn_salir.Size = new System.Drawing.Size(26, 25);
            this.btn_salir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_salir.TabIndex = 27;
            this.btn_salir.TabStop = false;
            this.btn_salir.Click += new System.EventHandler(this.btn_salir_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(51, 763);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 22);
            this.label3.TabIndex = 34;
            this.label3.Text = "Regresar";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_regresar
            // 
            this.btn_regresar.Image = ((System.Drawing.Image)(resources.GetObject("btn_regresar.Image")));
            this.btn_regresar.Location = new System.Drawing.Point(10, 758);
            this.btn_regresar.Name = "btn_regresar";
            this.btn_regresar.Size = new System.Drawing.Size(41, 31);
            this.btn_regresar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_regresar.TabIndex = 33;
            this.btn_regresar.TabStop = false;
            this.btn_regresar.Click += new System.EventHandler(this.btn_regresar_Click);
            // 
            // Reporte1
            // 
            chartArea1.Name = "ChartArea1";
            this.Reporte1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.Reporte1.Legends.Add(legend1);
            this.Reporte1.Location = new System.Drawing.Point(81, 183);
            this.Reporte1.Margin = new System.Windows.Forms.Padding(2);
            this.Reporte1.Name = "Reporte1";
            this.Reporte1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SemiTransparent;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.Label = "#PERCENT{P0}";
            series1.LabelToolTip = "#VALX";
            series1.Legend = "Legend1";
            series1.LegendText = "#VALX";
            series1.LegendToolTip = "#PERCENT{P0}";
            series1.Name = "Series1";
            this.Reporte1.Series.Add(series1);
            this.Reporte1.Size = new System.Drawing.Size(250, 150);
            this.Reporte1.TabIndex = 35;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(136, 146);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 22);
            this.label2.TabIndex = 36;
            this.label2.Text = "Primer Reporte";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(774, 146);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 22);
            this.label4.TabIndex = 37;
            this.label4.Text = "Tercer Reporte";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grafrepo3
            // 
            chartArea2.Name = "ChartArea1";
            this.grafrepo3.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.grafrepo3.Legends.Add(legend2);
            this.grafrepo3.Location = new System.Drawing.Point(713, 183);
            this.grafrepo3.Margin = new System.Windows.Forms.Padding(2);
            this.grafrepo3.Name = "grafrepo3";
            this.grafrepo3.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Excel;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series2.Label = "#PERCENT{P0}";
            series2.LabelToolTip = "#VALX";
            series2.Legend = "Legend1";
            series2.LegendText = "#VALX";
            series2.LegendToolTip = "#PERCENT{P0}";
            series2.Name = "Series1";
            this.grafrepo3.Series.Add(series2);
            this.grafrepo3.Size = new System.Drawing.Size(250, 150);
            this.grafrepo3.TabIndex = 38;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(463, 146);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 22);
            this.label5.TabIndex = 40;
            this.label5.Text = "Segundo Reporte";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grafico_reporte_cuatro
            // 
            chartArea3.Name = "ChartArea1";
            this.grafico_reporte_cuatro.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.grafico_reporte_cuatro.Legends.Add(legend3);
            this.grafico_reporte_cuatro.Location = new System.Drawing.Point(81, 437);
            this.grafico_reporte_cuatro.Name = "grafico_reporte_cuatro";
            this.grafico_reporte_cuatro.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Chocolate;
            this.grafico_reporte_cuatro.Size = new System.Drawing.Size(386, 171);
            this.grafico_reporte_cuatro.TabIndex = 43;
            this.grafico_reporte_cuatro.Text = "chart1";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(199, 344);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(152, 22);
            this.label6.TabIndex = 44;
            this.label6.Text = "Cuarto Reporte";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbx_provincias
            // 
            this.cbx_provincias.FormattingEnabled = true;
            this.cbx_provincias.Items.AddRange(new object[] {
            "San José",
            "Alajuela",
            "Heredia",
            "Cartago",
            "Guanacaste",
            "Puntarenas",
            "Limón"});
            this.cbx_provincias.Location = new System.Drawing.Point(81, 376);
            this.cbx_provincias.Name = "cbx_provincias";
            this.cbx_provincias.Size = new System.Drawing.Size(109, 21);
            this.cbx_provincias.TabIndex = 46;
            // 
            // rb_masculino
            // 
            this.rb_masculino.AutoSize = true;
            this.rb_masculino.Location = new System.Drawing.Point(202, 377);
            this.rb_masculino.Name = "rb_masculino";
            this.rb_masculino.Size = new System.Drawing.Size(73, 17);
            this.rb_masculino.TabIndex = 48;
            this.rb_masculino.TabStop = true;
            this.rb_masculino.Text = "Masculino";
            this.rb_masculino.UseVisualStyleBackColor = true;
            // 
            // rb_femenino
            // 
            this.rb_femenino.AutoSize = true;
            this.rb_femenino.Location = new System.Drawing.Point(281, 377);
            this.rb_femenino.Name = "rb_femenino";
            this.rb_femenino.Size = new System.Drawing.Size(71, 17);
            this.rb_femenino.TabIndex = 49;
            this.rb_femenino.TabStop = true;
            this.rb_femenino.Text = "Femenino";
            this.rb_femenino.UseVisualStyleBackColor = true;
            // 
            // btn_generar_reporte_4
            // 
            this.btn_generar_reporte_4.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn_generar_reporte_4.FlatAppearance.BorderColor = System.Drawing.Color.DodgerBlue;
            this.btn_generar_reporte_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_generar_reporte_4.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_generar_reporte_4.ForeColor = System.Drawing.Color.White;
            this.btn_generar_reporte_4.Location = new System.Drawing.Point(81, 399);
            this.btn_generar_reporte_4.Name = "btn_generar_reporte_4";
            this.btn_generar_reporte_4.Size = new System.Drawing.Size(386, 23);
            this.btn_generar_reporte_4.TabIndex = 50;
            this.btn_generar_reporte_4.Text = "Generar Reporte";
            this.btn_generar_reporte_4.UseVisualStyleBackColor = false;
            this.btn_generar_reporte_4.Click += new System.EventHandler(this.btn_generar_reporte_4_Click);
            // 
            // grafico_reporte_dos
            // 
            chartArea4.Name = "ChartArea1";
            this.grafico_reporte_dos.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.grafico_reporte_dos.Legends.Add(legend4);
            this.grafico_reporte_dos.Location = new System.Drawing.Point(396, 183);
            this.grafico_reporte_dos.Name = "grafico_reporte_dos";
            this.grafico_reporte_dos.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SeaGreen;
            this.grafico_reporte_dos.Size = new System.Drawing.Size(250, 150);
            this.grafico_reporte_dos.TabIndex = 51;
            this.grafico_reporte_dos.Text = "chart1";
            // 
            // grafico_reporte_cinco
            // 
            chartArea5.Name = "ChartArea1";
            this.grafico_reporte_cinco.ChartAreas.Add(chartArea5);
            legend5.Name = "Legend1";
            this.grafico_reporte_cinco.Legends.Add(legend5);
            this.grafico_reporte_cinco.Location = new System.Drawing.Point(577, 437);
            this.grafico_reporte_cinco.Name = "grafico_reporte_cinco";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series3.Legend = "Legend1";
            series3.LegendText = "#PERCENT{P0}";
            series3.LegendToolTip = "#VALX";
            series3.Name = "Dosis";
            this.grafico_reporte_cinco.Series.Add(series3);
            this.grafico_reporte_cinco.Size = new System.Drawing.Size(386, 171);
            this.grafico_reporte_cinco.TabIndex = 52;
            this.grafico_reporte_cinco.Text = "chart1";
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Location = new System.Drawing.Point(703, 344);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(152, 22);
            this.label7.TabIndex = 53;
            this.label7.Text = "Quinto Reporte";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_generar_reporte_cinco
            // 
            this.btn_generar_reporte_cinco.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn_generar_reporte_cinco.FlatAppearance.BorderColor = System.Drawing.Color.DodgerBlue;
            this.btn_generar_reporte_cinco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_generar_reporte_cinco.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_generar_reporte_cinco.ForeColor = System.Drawing.Color.White;
            this.btn_generar_reporte_cinco.Location = new System.Drawing.Point(577, 399);
            this.btn_generar_reporte_cinco.Name = "btn_generar_reporte_cinco";
            this.btn_generar_reporte_cinco.Size = new System.Drawing.Size(386, 23);
            this.btn_generar_reporte_cinco.TabIndex = 54;
            this.btn_generar_reporte_cinco.Text = "Generar Reporte";
            this.btn_generar_reporte_cinco.UseVisualStyleBackColor = false;
            this.btn_generar_reporte_cinco.Click += new System.EventHandler(this.btn_generar_reporte_cinco_Click);
            // 
            // cbx_provincia
            // 
            this.cbx_provincia.FormattingEnabled = true;
            this.cbx_provincia.Items.AddRange(new object[] {
            "San José",
            "Alajuela",
            "Heredia",
            "Cartago",
            "Guanacaste",
            "Puntarenas",
            "Limón"});
            this.cbx_provincia.Location = new System.Drawing.Point(577, 375);
            this.cbx_provincia.Name = "cbx_provincia";
            this.cbx_provincia.Size = new System.Drawing.Size(386, 21);
            this.cbx_provincia.TabIndex = 55;
            this.cbx_provincia.SelectedIndexChanged += new System.EventHandler(this.cbx_provincia_SelectedIndexChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(17, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(109, 94);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 56;
            this.pictureBox1.TabStop = false;
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnFiltrar.FlatAppearance.BorderColor = System.Drawing.Color.DodgerBlue;
            this.btnFiltrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFiltrar.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFiltrar.ForeColor = System.Drawing.Color.White;
            this.btnFiltrar.Location = new System.Drawing.Point(675, 654);
            this.btnFiltrar.Margin = new System.Windows.Forms.Padding(2);
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Size = new System.Drawing.Size(240, 22);
            this.btnFiltrar.TabIndex = 64;
            this.btnFiltrar.Text = "Filtrar";
            this.btnFiltrar.UseVisualStyleBackColor = false;
            this.btnFiltrar.Click += new System.EventHandler(this.btnFiltrar_Click);
            // 
            // dateTimeHasta
            // 
            this.dateTimeHasta.Location = new System.Drawing.Point(476, 654);
            this.dateTimeHasta.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimeHasta.Name = "dateTimeHasta";
            this.dateTimeHasta.Size = new System.Drawing.Size(195, 20);
            this.dateTimeHasta.TabIndex = 63;
            // 
            // dateTimeDesde
            // 
            this.dateTimeDesde.Location = new System.Drawing.Point(220, 654);
            this.dateTimeDesde.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimeDesde.Name = "dateTimeDesde";
            this.dateTimeDesde.Size = new System.Drawing.Size(195, 20);
            this.dateTimeDesde.TabIndex = 62;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(427, 652);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 22);
            this.label8.TabIndex = 61;
            this.label8.Text = "Hasta:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(165, 652);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 22);
            this.label9.TabIndex = 60;
            this.label9.Text = "Desde:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DataGridSextorepo
            // 
            this.DataGridSextorepo.AllowUserToAddRows = false;
            this.DataGridSextorepo.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DataGridSextorepo.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridSextorepo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataGridSextorepo.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridSextorepo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridSextorepo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridSextorepo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Cedula,
            this.Nombre,
            this.Genero,
            this.Fecha_Nacimiento,
            this.Tipo_Vacuna,
            this.Cantidad_Dosis,
            this.Fecha_Segunda_Dosis});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridSextorepo.DefaultCellStyle = dataGridViewCellStyle10;
            this.DataGridSextorepo.Location = new System.Drawing.Point(168, 678);
            this.DataGridSextorepo.Margin = new System.Windows.Forms.Padding(2);
            this.DataGridSextorepo.Name = "DataGridSextorepo";
            this.DataGridSextorepo.ReadOnly = true;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridSextorepo.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.DataGridSextorepo.RowHeadersVisible = false;
            this.DataGridSextorepo.RowHeadersWidth = 51;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DataGridSextorepo.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.DataGridSextorepo.RowTemplate.Height = 24;
            this.DataGridSextorepo.Size = new System.Drawing.Size(747, 108);
            this.DataGridSextorepo.TabIndex = 57;
            // 
            // Cedula
            // 
            this.Cedula.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cedula.DefaultCellStyle = dataGridViewCellStyle3;
            this.Cedula.Frozen = true;
            this.Cedula.HeaderText = "Cedula";
            this.Cedula.Name = "Cedula";
            this.Cedula.ReadOnly = true;
            this.Cedula.Width = 69;
            // 
            // Nombre
            // 
            this.Nombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F);
            this.Nombre.DefaultCellStyle = dataGridViewCellStyle4;
            this.Nombre.Frozen = true;
            this.Nombre.HeaderText = "Nombre";
            this.Nombre.Name = "Nombre";
            this.Nombre.ReadOnly = true;
            this.Nombre.Width = 74;
            // 
            // Genero
            // 
            this.Genero.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F);
            this.Genero.DefaultCellStyle = dataGridViewCellStyle5;
            this.Genero.Frozen = true;
            this.Genero.HeaderText = "Género";
            this.Genero.Name = "Genero";
            this.Genero.ReadOnly = true;
            this.Genero.Width = 72;
            // 
            // Fecha_Nacimiento
            // 
            this.Fecha_Nacimiento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F);
            this.Fecha_Nacimiento.DefaultCellStyle = dataGridViewCellStyle6;
            this.Fecha_Nacimiento.Frozen = true;
            this.Fecha_Nacimiento.HeaderText = "Fecha Nacimiento";
            this.Fecha_Nacimiento.Name = "Fecha_Nacimiento";
            this.Fecha_Nacimiento.ReadOnly = true;
            this.Fecha_Nacimiento.Width = 135;
            // 
            // Tipo_Vacuna
            // 
            this.Tipo_Vacuna.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F);
            this.Tipo_Vacuna.DefaultCellStyle = dataGridViewCellStyle7;
            this.Tipo_Vacuna.Frozen = true;
            this.Tipo_Vacuna.HeaderText = "Tipo Vacuna";
            this.Tipo_Vacuna.Name = "Tipo_Vacuna";
            this.Tipo_Vacuna.ReadOnly = true;
            this.Tipo_Vacuna.Width = 110;
            // 
            // Cantidad_Dosis
            // 
            this.Cantidad_Dosis.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F);
            this.Cantidad_Dosis.DefaultCellStyle = dataGridViewCellStyle8;
            this.Cantidad_Dosis.Frozen = true;
            this.Cantidad_Dosis.HeaderText = "Cantidad de Dosis";
            this.Cantidad_Dosis.Name = "Cantidad_Dosis";
            this.Cantidad_Dosis.ReadOnly = true;
            this.Cantidad_Dosis.Width = 135;
            // 
            // Fecha_Segunda_Dosis
            // 
            this.Fecha_Segunda_Dosis.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F);
            this.Fecha_Segunda_Dosis.DefaultCellStyle = dataGridViewCellStyle9;
            this.Fecha_Segunda_Dosis.Frozen = true;
            this.Fecha_Segunda_Dosis.HeaderText = "Fecha Segunda Dosis";
            this.Fecha_Segunda_Dosis.Name = "Fecha_Segunda_Dosis";
            this.Fecha_Segunda_Dosis.ReadOnly = true;
            this.Fecha_Segunda_Dosis.Width = 150;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label12.Location = new System.Drawing.Point(463, 624);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(152, 22);
            this.label12.TabIndex = 65;
            this.label12.Text = "Sexto Reporte";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbx_tipo_vacuna
            // 
            this.cbx_tipo_vacuna.FormattingEnabled = true;
            this.cbx_tipo_vacuna.Items.AddRange(new object[] {
            "AstraZeneca",
            "Pfizer",
            "Janssen (J&J)",
            "SINOVAC",
            "Sputnik V"});
            this.cbx_tipo_vacuna.Location = new System.Drawing.Point(358, 375);
            this.cbx_tipo_vacuna.Name = "cbx_tipo_vacuna";
            this.cbx_tipo_vacuna.Size = new System.Drawing.Size(109, 21);
            this.cbx_tipo_vacuna.TabIndex = 66;
            // 
            // Form_Reportes_Administrador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1048, 810);
            this.Controls.Add(this.cbx_tipo_vacuna);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.btnFiltrar);
            this.Controls.Add(this.dateTimeHasta);
            this.Controls.Add(this.dateTimeDesde);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.DataGridSextorepo);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cbx_provincia);
            this.Controls.Add(this.btn_generar_reporte_cinco);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.grafico_reporte_cinco);
            this.Controls.Add(this.grafico_reporte_dos);
            this.Controls.Add(this.btn_generar_reporte_4);
            this.Controls.Add(this.rb_femenino);
            this.Controls.Add(this.rb_masculino);
            this.Controls.Add(this.cbx_provincias);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.grafico_reporte_cuatro);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.grafrepo3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Reporte1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_regresar);
            this.Controls.Add(this.btn_salir);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label11);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form_Reportes_Administrador";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form_Reportes_Administrador_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btn_salir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_regresar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Reporte1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grafrepo3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grafico_reporte_cuatro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grafico_reporte_dos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grafico_reporte_cinco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridSextorepo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox btn_salir;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox btn_regresar;
        private System.Windows.Forms.DataVisualization.Charting.Chart Reporte1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataVisualization.Charting.Chart grafrepo3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataVisualization.Charting.Chart grafico_reporte_cuatro;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbx_provincias;
        private System.Windows.Forms.RadioButton rb_masculino;
        private System.Windows.Forms.RadioButton rb_femenino;
        private System.Windows.Forms.Button btn_generar_reporte_4;
        private System.Windows.Forms.DataVisualization.Charting.Chart grafico_reporte_dos;
        private System.Windows.Forms.DataVisualization.Charting.Chart grafico_reporte_cinco;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_generar_reporte_cinco;
        private System.Windows.Forms.ComboBox cbx_provincia;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnFiltrar;
        private System.Windows.Forms.DateTimePicker dateTimeHasta;
        private System.Windows.Forms.DateTimePicker dateTimeDesde;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView DataGridSextorepo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbx_tipo_vacuna;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cedula;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Genero;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha_Nacimiento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tipo_Vacuna;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad_Dosis;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha_Segunda_Dosis;
    }
}