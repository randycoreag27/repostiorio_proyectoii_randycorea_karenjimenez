﻿using Capa_Negocio;
using Objetos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Capa_Presentacion
{
    public partial class Form_Registro_Vacunacion : Form
    {
        List<Objeto_Informacion_Persona> listaRep1 = new List<Objeto_Informacion_Persona>();//lista que almacena toda la info solcitada en capa negocio
        XML_Registro_Vacunacion xml = new XML_Registro_Vacunacion(); //Instacia de la clase donde se realiza el proceso del archivo XML
        string genero = "";
        string numero_dosis = "";

        public Form_Registro_Vacunacion()
        {
            InitializeComponent();
            CrearXML();
            xml.LeerXML();//lectura a xml
        }
        //Crear el archivo xml o bien consultar si ya existe
        public void CrearXML()
        {
            string ruta = "Registro_Vacunacion.xml";
            if (!File.Exists(ruta))
            {
                xml.CrearXML(ruta, "Información");
            }
            else
            {
                xml.LeerXML();
            }
        }

        //Método para limpiar campos de interfaz gráfica
        public void Limpiar()
        {
            txtCedula.Clear();
            txtNombre.Clear();
            rbFemenino.Checked = true;
            rbUnaDosis.Checked = true;
        }

        //Método en el cual se obtinen los datos de la interfaz gráfica
        public void Obtener_Datos()
        {
            //genero
            if (rbFemenino.Checked)
            {
                genero = "Femenino";
            }
            else if (rbMasculino.Checked)
            {
                genero = "Masculino";
            }

            //Cantidad_Dosis
            if (rbUnaDosis.Checked)
            {
                numero_dosis = "Una Dosis";
            }
            else if (rbDosDosis.Checked)
            {
                numero_dosis = "Dos Dosis";
            }

            //calculo edad
            DateTime fechaaActual = System.DateTime.Today;
            int año_actual = fechaaActual.Year;
            DateTime fechaN = DateFecha_nacimiento.Value.Date;
            int año_naci = fechaN.Year;
            int edad = año_actual - año_naci;
            Objeto_Informacion_Persona obj = new Objeto_Informacion_Persona()//Instacia de la clase donde se encuentra el objeto de la información de la persona
            {
                cedula = Convert.ToInt32(txtCedula.Text),
                nombre = txtNombre.Text,
                genero = genero,
                fecha_nacimiento = (DateFecha_nacimiento.Value.Day + "/" + DateFecha_nacimiento.Value.Month + "/" + DateFecha_nacimiento.Value.Year),
                provincia = Convert.ToString(comboProvincia.SelectedItem),
                tipo_vacuna = Convert.ToString(comboTipoVacuna.SelectedItem),
                cantidad_dosis = numero_dosis,
                fecha_primera_dosis = (DateFecha1Dosis.Value.Day + "/" + DateFecha1Dosis.Value.Month + "/" + DateFecha1Dosis.Value.Year),
                fecha_segunda_dosis = (DateFecha2Dosis.Value.Day + "/" + DateFecha2Dosis.Value.Month + "/" + DateFecha2Dosis.Value.Year),
                edad = edad
            };
            if (edad < 12)
            {
                MessageBox.Show("La persona vacunada debe ser mayor de 12años", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                xml.Registro_Vacunacion(obj);//envio informacion a archivo
                Form_MessageBox_InserccionDatos msb = new Form_MessageBox_InserccionDatos();
                msb.Show();
            }
        }


        //Método en el cual se realiza el proceso a llevar para insertar los datos de la persona en el archivo XML
        public void Registro()
        {
            Obtener_Datos();
            Limpiar();
        }

        private void btn_iniciar_sesion_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form_Inicio_Sesion frm = new Form_Inicio_Sesion();
            frm.Show();
        }

        private void btn_registrar_Click(object sender, EventArgs e)
        {
            DateTime fecha1dosis = DateFecha1Dosis.Value.Date;
            DateTime fecha2dosis = DateFecha2Dosis.Value.Date;
            DateTime fechaaActual = System.DateTime.Today;
            DateTime fechaNaci = DateFecha_nacimiento.Value.Date;
            listaRep1 = xml.Cargar_Datos_Reportes();
            if (txtCedula.Text == "" || txtNombre.Text == "" || comboProvincia.SelectedIndex.Equals(-1) || comboTipoVacuna.SelectedIndex.Equals(-1))//si los combos no están seleccionados...
            {
                MessageBox.Show("Debe ingresar todos los datos solicitados", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (fechaNaci >= fechaaActual)
            {
                MessageBox.Show("Verifica que la fecha de nacimiento esté correcta", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (rbDosDosis.Checked && fecha1dosis >= fecha2dosis)
            {
                MessageBox.Show("Error, la fecha de la segunda dosis debe ser mayor", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                bool existe = false;
                foreach (Objeto_Informacion_Persona obj in listaRep1)
                {
                    if (obj.cedula == Convert.ToInt32(txtCedula.Text))
                    {
                        existe = true;
                        MessageBox.Show("El usuario ya existe, intentelo nuevamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                if (existe == false)
                {
                    Registro();
                }


            }
        }

        private void btn_salir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void rbUnaDosis_CheckedChanged(object sender, EventArgs e)
        {
            if (rbUnaDosis.Checked)
            {
                DateFecha2Dosis.Enabled = false;
            }
            else
            {
                DateFecha2Dosis.Enabled = true;
            }
        }

        private void rbDosDosis_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void txtCedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo se pueden ingresar números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Handled = true;
                return;
            }
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 33 && e.KeyChar <= 57) || (e.KeyChar >= 91 && e.KeyChar <= 96) || (e.KeyChar >= 123 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo se pueden ingresar letras", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Handled = true;
                return;
            }
        }
    }
}
