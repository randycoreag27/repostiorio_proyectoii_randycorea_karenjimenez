﻿using Capa_Negocio;
using Objetos;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Capa_Presentacion
{
    public partial class Form_Reportes_Administrador : Form
    {
        XML_Registro_Vacunacion xml = new XML_Registro_Vacunacion();//instancia a la capa de negocio
        List<Objeto_Informacion_Persona> listaReportes = new List<Objeto_Informacion_Persona>();//lista que almacena toda la info solcitada en capa negocio

        public Form_Reportes_Administrador()
        {
            InitializeComponent();
            xml.LeerXML();//lectura a xml
        }

        //Reporte 1
        //mostrar el porcentaje de personas por provincia que
        //han registrado su información de vacunación.
        public void Primer_Reporte()
        {
            listaReportes = xml.Cargar_Datos_Reportes();
            //contadores por provincia
            int provincia = 0;//contador encargado de calcular el total de provincias
            float Alajuela = 0;
            float San_Jose = 0;
            float Heredia = 0;
            float Cartago = 0;
            float Guanacaste = 0;
            float Puntarenas = 0;
            float Limon = 0;
            for (int i = 0; i < listaReportes.Count; i++)
            {
                if (listaReportes[i].provincia.Equals("San José"))
                {
                    San_Jose++;
                    provincia++;

                }
                else if (listaReportes[i].provincia.Equals("Alajuela"))
                {
                    provincia++;
                    Alajuela++;

                }
                else if (listaReportes[i].provincia.Equals("Heredia"))
                {
                    provincia++;
                    Heredia++;

                }
                else if (listaReportes[i].provincia.Equals("Cartago"))
                {
                    provincia++;
                    Cartago++;
                }
                else if (listaReportes[i].provincia.Equals("Guanacaste"))
                {
                    provincia++;
                    Guanacaste++;

                }
                else if (listaReportes[i].provincia.Equals("Puntarenas"))
                {
                    provincia++;
                    Puntarenas++;

                }
                else if (listaReportes[i].provincia.Equals("Limón"))
                {
                    provincia++;
                    Limon++;

                }
            }
            //Cálculo de porcentajes
            San_Jose = (San_Jose * provincia) / 100;
            Alajuela = (Alajuela * provincia) / 100;
            Cartago = (Cartago * provincia) / 100;
            Heredia = (Heredia * provincia) / 100;
            Guanacaste = (Guanacaste * provincia) / 100;
            Puntarenas = (Puntarenas * provincia) / 100;
            Limon = (Limon * provincia) / 100;
            string[] provincias = { "San José", "Alajuela", "Heredia", "Cartago", "Guanacaste", "Puntarenas", "Limón" };
            float[] valores = { San_Jose, Alajuela, Heredia, Cartago, Guanacaste, Puntarenas, Limon };
            for (int i = 0; i < provincias.Length; i++)
            { //recorrido del arreglo de provincias
                if (valores[i] > 0)//validación 
                {
                    Reporte1.Series["Series1"].Points.AddXY(provincias[i], valores[i]);//inserta datos en el gráfico
                }
            }

        }

        //Reporte 2
        //Cantidad de personas por género que han registrado su
        //información de vacunación contemplando todas las provincias.
        public void Segundo_Reporte()
        {
            //Lista con los datos de las personas vacunadas
            listaReportes = xml.Cargar_Datos_Reportes();

            //Variables para llevar el control de la cantidad de personas por género
            int cantidad_hombres=0;
            int cantidad_mujeres = 0;

            //For para recorrer la lista que contiene la información de vacunación
            for (int i = 0; i < listaReportes.Count; i++)
            {
                //Condiciones para validar la información y añadir a los graficos
                if (listaReportes[i].genero.Equals("Masculino"))
                {
                    cantidad_hombres++;
                }
                //Condiciones para validar la información y añadir a los graficos
                else if (listaReportes[i].genero.Equals("Femenino"))
                {
                    cantidad_mujeres++;
                }
            }

            //Variables para así manejar la información correspondiente al reporte
            string[] series = {"Hombres", "Mujeres"};
            int[] cantidad = {cantidad_hombres, cantidad_mujeres};

            //For para recorrer la información extraida y mostrarla en el gráfico respectivo
            for (int i = 0; i < series.Length; i++)
            {
                Series serie = grafico_reporte_dos.Series.Add(series[i]);
                serie.Label = cantidad[i].ToString();
                serie.Points.Add(cantidad[i]);
            }
        }

        //Reporte 3
        //porcentaje de personas que han registrado su información de vacunación de acuerdo con los
        //siguientes rangos de edades ya definidos: entre 12 y 19, entre 20 y 29, entre 30 y 39, entre 40 y 49, más de 50.
        public void Tercer_Reporte()
        {
            listaReportes = xml.Cargar_Datos_Reportes();
            //contadores por edades
            int num_edades_per = 0;//contador encargado de calcular el total de edades
            float Repo1 = 0;//12-19
            float Repo2 = 0;//20-29
            float Repo3 = 0;//30-39
            float Repo4 = 0;//40-49
            float Repo5 = 0;//>50
            for (int i = 0; i < listaReportes.Count; i++)
            {
                int edad_conv = listaReportes[i].edad;
                if (edad_conv >= 12 && edad_conv <= 19)
                {
                    Repo1++;
                    num_edades_per++;

                }
                else if (edad_conv >= 20 && edad_conv <= 29)
                {
                    num_edades_per++;
                    Repo2++;

                }
                else if (edad_conv >= 30 && edad_conv <= 39)
                {
                    num_edades_per++;
                    Repo3++;

                }
                else if (edad_conv >= 40 && edad_conv <= 49)
                {
                    num_edades_per++;
                    Repo4++;
                }
                else if (edad_conv >= 50)
                {
                    num_edades_per++;
                    Repo5++;

                }
            }
            //Cálculo de porcentajes
            Repo1 = (Repo1 * num_edades_per) / 100;
            Repo2 = (Repo2 * num_edades_per) / 100;
            Repo3 = (Repo3 * num_edades_per) / 100;
            Repo4 = (Repo4 * num_edades_per) / 100;
            Repo5 = (Repo5 * num_edades_per) / 100;
            string[] rangos = { "12-19", "20-29", "30-39", "40-49", ">50" };
            float[] valores = { Repo1, Repo2, Repo3, Repo4, Repo5 };
            for (int i = 0; i < rangos.Length; i++)
            { //recorrido del arreglo de provincias
                if (valores[i] > 0)//validación 
                {   
                    grafrepo3.Series["Series1"].Points.AddXY(rangos[i], valores[i]);//inserta datos en el gráfico
                }
            }
        }
        
        //Reporte 4
        //mostrar la cantidad de personas que han registrado su información
        //de vacunación por provincia, por género y por un tipo de vacuna
        public void Cuarto_Reporte()
        {
            //Variables creadas para el manejo de la información de interfaz
            string provincia = Convert.ToString(cbx_provincias.SelectedItem);
            string genero = "";
            string tipo_vacuna = Convert.ToString(cbx_tipo_vacuna.SelectedItem);
            int cantidad_personas = 0;

            //Lista con la información de vacunación
            listaReportes = xml.Cargar_Datos_Reportes();

            //Condición para saber que valor va a tomar según el RadioButton seleccionado
            if (rb_masculino.Checked)
            {
                genero = "Masculino";
            }
            //Condición para saber que valor va a tomar según el RadioButton seleccionado
            if (rb_femenino.Checked)
            {
                genero = "Femenino";
            }

            for (int i = 0; i < listaReportes.Count; i++)
            {
                if(listaReportes[i].provincia.Equals(provincia) && listaReportes[i].genero.Equals(genero) 
                    && listaReportes[i].tipo_vacuna.Equals(tipo_vacuna))
                {
                    cantidad_personas++;
                }
            }

            int[] cantidad = { cantidad_personas };
            string[] datos = { provincia+" "+genero+" "+tipo_vacuna };

            //For para recorrer la información extraida y mostrarla en el gráfico respectivo
            for (int i = 0; i < datos.Length; i++)
            {
                if (cantidad[i] > 0)
                {
                    Series serie = grafico_reporte_cuatro.Series.Add(datos[i]);
                    serie.Label = cantidad[i].ToString();
                    serie.Points.Add(cantidad[i]);
                }
            }
        }

        //Reporte 5
        //Porcentaje de la cantidad de dosis (una o dos) de acuerdo con la  
        //información registrada por las personas de una provincia en específico
        public void Quinto_Reporte()
        {
            //Variables creadas para el manejo de la información de interfaz
            string provincia = Convert.ToString(cbx_provincia.SelectedItem);
            double una_dosis = 0;
            double dos_dosis = 0;

            //Lista con la información de vacunación
            listaReportes = xml.Cargar_Datos_Reportes();

            for (int i = 0; i < listaReportes.Count; i++)
            {
                if (listaReportes[i].provincia.Equals(provincia) && listaReportes[i].cantidad_dosis.Equals("Una Dosis"))
                {
                    una_dosis++;
                }
                else if (listaReportes[i].provincia.Equals(provincia) && listaReportes[i].cantidad_dosis.Equals("Dos Dosis"))
                {
                    dos_dosis++;
                }
            }

            una_dosis = (una_dosis * 1) / 100;
            dos_dosis = (dos_dosis * 1) / 100;

            string[] dosis = { "Una Dosis", "Dos Dosis" };
            double[] cantidad_dosis = { una_dosis, dos_dosis };

            //For para recorrer la información extraida y mostrarla en el gráfico respectivo
            for (int i = 0; i < dosis.Length; i++)
            {
                if (cantidad_dosis[i] > 0)
                {
                    grafico_reporte_cinco.Series["Dosis"].Points.AddXY(dosis[i], cantidad_dosis[i]);
                }
            }

        }

        //Reporte 6
        //Mostrar la lista de personas con las dos dosis de
        //vacunación, de acuerdo con un rango de fecha
        public void Sexto_Reporte()
        {
            listaReportes = xml.Cargar_Datos_Reportes();
            for (int i = 0; i < listaReportes.Count; i++)
            {
                if (listaReportes[i].fecha_segunda_dosis != "Sin Aplicar")
                {
                    string fecha = listaReportes[i].fecha_segunda_dosis;
                    DateTime oDate = Convert.ToDateTime(fecha);

                    if (listaReportes[i].cantidad_dosis.Equals("Dos Dosis")
                        && (dateTimeDesde.Value >= oDate || dateTimeDesde.Value <= oDate)
                        && oDate <= dateTimeHasta.Value)
                    {
                        DataGridViewRow fila = new DataGridViewRow();
                        fila.CreateCells(this.DataGridSextorepo);
                        fila.Cells[0].Value = listaReportes[i].cedula;
                        fila.Cells[1].Value = listaReportes[i].nombre;
                        fila.Cells[2].Value = listaReportes[i].genero;
                        fila.Cells[3].Value = listaReportes[i].fecha_nacimiento;
                        fila.Cells[4].Value = listaReportes[i].tipo_vacuna;
                        fila.Cells[5].Value = listaReportes[i].cantidad_dosis;
                        fila.Cells[6].Value = listaReportes[i].fecha_segunda_dosis;
                        DataGridSextorepo.Rows.Add(fila);
                    }
                }
            }
        }
        private void Form_Reportes_Administrador_Load(object sender, EventArgs e)
        {
            //agrega titulo al primer gráfico
            Reporte1.Titles.Add(" Porcentaje de personas por provincia ");
            Primer_Reporte();
            //agrega titulo al primer gráfico
            grafico_reporte_dos.Titles.Add("Cantidad de Personas Vacunadas por Género");
            Segundo_Reporte();
            //agrega titulo al tercer gráfico 
            grafrepo3.Titles.Add(" Porcentaje de personas por provincia según su edad  ");
            Tercer_Reporte();

            //Titulo Correspondiente al Guarto Gráfico
            grafico_reporte_cuatro.Titles.Add("Cantidad de Personas por Provincia, Género y Tipo de Vacuna");

            //Titulo Correspondiente al Guarto Gráfico
            grafico_reporte_cinco.Titles.Add("Porcentaje de Cantidad de Dosis por Provincia");

        }
        private void btn_salir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void btn_regresar_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form_Inicio_Sesion frm = new Form_Inicio_Sesion();
            frm.Show();
        }
        private void btn_generar_reporte_4_Click(object sender, EventArgs e)
        {
            if(cbx_provincias.SelectedItem == null && cbx_tipo_vacuna.SelectedItem == null)
            {
                Form_MessageBox_Aviso_Reportes msb = new Form_MessageBox_Aviso_Reportes();
                msb.Show();
            }
            //Invocación al Método que Realiza el Reporte 4
            Cuarto_Reporte();
        }
        private void btn_generar_reporte_cinco_Click(object sender, EventArgs e)
        {
            if(cbx_provincia.SelectedItem == null)
            {
                Form_MessageBox_Aviso_Reportes msb = new Form_MessageBox_Aviso_Reportes();
                msb.Show();
            }
            else
            {
                //Invocación al Método que Realiza el Reporte 5
                Quinto_Reporte();
            }
        }
        private void btnFiltrar_Click(object sender, EventArgs e)
        {

            if (dateTimeHasta.Value > dateTimeDesde.Value)
            {
                DataGridSextorepo.Rows.Clear();
                Sexto_Reporte();
            }
            else
            {
                MessageBox.Show("Verifique el Rango de Fechas Ingresado", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void cbx_provincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            Quinto_Reporte();
        }
    }
}
