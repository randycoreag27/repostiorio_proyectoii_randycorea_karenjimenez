﻿using System;
using System.Collections.Generic;
using System.Xml;
using Capa_Datos;
using Objetos;

namespace Capa_Negocio
{
    public class XML_Registro_Vacunacion
    {
        XmlDocument doc;
        string rutaXml= "Registro_Vacunacion.xml";

        //creación del xml
        public void CrearXML(string ruta, string nodoRaiz)
        {
            rutaXml = ruta;
            doc = new XmlDocument();

            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", "no");//declaracion

            XmlNode nodo = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, nodo);

            XmlNode elemento = doc.CreateElement(nodoRaiz);
            doc.AppendChild(elemento);

            new Guardar_XML().guardarDatosXML(doc, ruta);
        }

        //Lectura
        public void LeerXML()
        {
            doc = new XmlDocument();
            doc.Load(rutaXml);//lee
        }

        //Registro
        public void Registro_Vacunacion(Objeto_Informacion_Persona obj)
        {
            XmlNode usuario = Crear_Registro_Vacunacion(obj);

            XmlNode nodo = doc.DocumentElement;

            nodo.InsertAfter(usuario, nodo.LastChild);

            new Guardar_XML().guardarDatosXML(doc, rutaXml);
        }

        //Orden de Insertar datos en el archivo
        public XmlNode Crear_Registro_Vacunacion(Objeto_Informacion_Persona obj)
        {
            //< Datos_Personales >
            XmlNode datos = doc.CreateElement("Datos_Personales");

            XmlElement xcedula = doc.CreateElement("Cédula");
            xcedula.InnerText = obj.cedula.ToString();
            datos.AppendChild(xcedula);

            XmlElement xnombre = doc.CreateElement("Nombre");
            xnombre.InnerText = obj.nombre.ToString();
            datos.AppendChild(xnombre);

            XmlElement xgenero = doc.CreateElement("Género");
            xgenero.InnerText = obj.genero.ToString();
            datos.AppendChild(xgenero);

            XmlElement xfechanacimiento = doc.CreateElement("Fecha_Nacimiento");
            xfechanacimiento.InnerText = obj.fecha_nacimiento.ToString();
            datos.AppendChild(xfechanacimiento);

            XmlElement xedad = doc.CreateElement("Edad");
            xedad.InnerText = obj.edad.ToString();
            datos.AppendChild(xedad);

            XmlElement xprovincia = doc.CreateElement("Provincia");
            xprovincia.InnerText = obj.provincia.ToString();
            datos.AppendChild(xprovincia);
            //</Datos_Personales> 

            //<Esquema_Vacunación> 
            XmlElement esquema = doc.CreateElement("Esquema_Vacunación");
            datos.AppendChild(esquema);

            XmlElement xtipo = doc.CreateElement("Tipo_Vacuna");
            xtipo.InnerText = obj.tipo_vacuna.ToString();
            esquema.AppendChild(xtipo);

            XmlElement xcantidad = doc.CreateElement("Cantidad_Dosis");
            xcantidad.InnerText = obj.cantidad_dosis.ToString();
            esquema.AppendChild(xcantidad);

            XmlElement x1fecha = doc.CreateElement("Fecha_Primera_Dosis");
            x1fecha.InnerText = obj.fecha_primera_dosis.ToString();
            esquema.AppendChild(x1fecha);

            if (obj.cantidad_dosis.Equals("Una Dosis"))
            {
                XmlElement x2fecha = doc.CreateElement("Fecha_Segunda_Dosis");
                x2fecha.InnerText = "Sin Aplicar";
                esquema.AppendChild(x2fecha);
            }
            else if(obj.cantidad_dosis.Equals("Dos Dosis"))
            {
                XmlElement x2fecha = doc.CreateElement("Fecha_Segunda_Dosis");
                x2fecha.InnerText = obj.fecha_segunda_dosis.ToString();
                esquema.AppendChild(x2fecha);
            }
            //</Esquema_Vacunación>

            return datos;
        }

        //Se extraen los datos para poder relealizar el inicio de sesion
        public List<Objeto_Usuario> Cargar_Datos_Login()
        {
            XmlNodeList listausers = doc.SelectNodes("Información/Datos_Usuarios");

            List<Objeto_Usuario> lista = new List<Objeto_Usuario>();

            XmlNode unUsuario;

            for (int i = 0; i < listausers.Count; i++)
            {
                unUsuario = listausers.Item(i);
                Objeto_Usuario usuario = new Objeto_Usuario
                {
                    cedula = Convert.ToInt32(unUsuario.SelectSingleNode("Cédula").InnerText),
                    contrasena = unUsuario.SelectSingleNode("Contraseña").InnerText,
                };
                lista.Add(usuario);
            }
            return lista;

        }

        //Se extraen los datos para de esa forma poder realizar los diferentes reportes
        public List<Objeto_Informacion_Persona> Cargar_Datos_Reportes()
        {

            List<Objeto_Informacion_Persona> lista = new List<Objeto_Informacion_Persona>();
            XmlNodeList lista_esquema_vacunacion = doc.SelectNodes("Información/Datos_Personales");

            foreach (XmlNode rep1 in lista_esquema_vacunacion)
            {
                Objeto_Informacion_Persona datos = new Objeto_Informacion_Persona
                {
                    cedula = Convert.ToInt32(rep1.SelectSingleNode("Cédula").InnerText),
                    nombre = rep1.SelectSingleNode("Nombre").InnerText,
                    genero = rep1.SelectSingleNode("Género").InnerText,
                    fecha_nacimiento = (rep1.SelectSingleNode("Fecha_Nacimiento").InnerText),
                    provincia = rep1.SelectSingleNode("Provincia").InnerText,
                    edad = Convert.ToInt32(rep1.SelectSingleNode("Edad").InnerText),
                    tipo_vacuna = rep1.SelectSingleNode("Esquema_Vacunación/Tipo_Vacuna").InnerText,
                    cantidad_dosis = rep1.SelectSingleNode("Esquema_Vacunación/Cantidad_Dosis").InnerText,
                    fecha_primera_dosis = (rep1.SelectSingleNode("Esquema_Vacunación/Fecha_Primera_Dosis").InnerText),
                    fecha_segunda_dosis = (rep1.SelectSingleNode("Esquema_Vacunación/Fecha_Segunda_Dosis").InnerText),
                };
                lista.Add(datos);
            }
            return lista;
        }
    }
}
